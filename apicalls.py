import requests
import json
from datetime import datetime


def left(s, amount):
    return s[:amount]


def right(s, amount):
    return s[-amount:]


def get_survey_responses(survey_id, question_id, response_ids, api_key):
    responses = []
    s = requests.session()
    s.headers.update({
        "Authorization": "Bearer %s" % api_key,
        "Content-Type": "application/json"
    })

    url = "https://api.surveymonkey.com/v3/surveys/%s/responses/bulk" % survey_id
    response = s.get(url)
    survey_responses = json.loads(response.content.decode())
    for survey_response in survey_responses['data']:
        for page in survey_response['pages']:
            for question in page['questions']:
                if question['id'] == question_id:
                    if len(question['answers'][0]['text']) <= 12 \
                            or left(question['answers'][0]['text'],3) != '26' \
                            or left(question['answers'][0]['text'], 1) == '+':
                        question['answers'][0]['text'] = '260' + right(question['answers'][0]['text'], 9)
                    if survey_response['id'] not in response_ids:
                        responses.append(
                            (question['answers'][0]['text'], survey_response['date_modified'], survey_response['id']))

    print(datetime.now(), "Survey Monkey Response code:", response.status_code)
    return responses


def send_phone_numbers(payload):
    s = requests.session()
    s.headers.update({
        "Content-Type": "application/json"
    })

    url = "http://192.168.105.189:88/index.php"
    response = s.post(url, data=json.dumps(payload))

    print(payload['tel'], payload['id'], "Code:", response.status_code, " Content:", response.content.decode("utf-8"))

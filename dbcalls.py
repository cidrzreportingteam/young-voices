import apicalls
import psycopg2
from datetime import datetime
from config import database_config


def insert_into_surveyresponses(survey_id, question_id, api_key):
    all_response_ids = []
    select_sql = "SELECT responseid FROM survey_monkey.yv_vitol_char_surveyresponses"
    insert_sql = "INSERT INTO survey_monkey.yv_vitol_char_surveyresponses(phonenumber, datesurveycompleted, responseid) values (%s, %s, %s) ON CONFLICT ON CONSTRAINT yv_vitol_char_surveyresponses_pkey DO NOTHING"
    conn = None
    try:
        params = database_config()
        print(datetime.now(), 'Connecting to the PostgreSQL database...')

        conn = psycopg2.connect(**params)
        cur = conn.cursor()

        print(datetime.now(), 'Retrieving existing responses saved in Young Voices database')

        cur.execute(select_sql)
        rows = cur.fetchall()
        for row in rows:
            all_response_ids.append(row[0])

        print(datetime.now(), len(all_response_ids), "responses currently saved in Survey Responses")
        print(datetime.now(), 'Retrieving all responses from Survey Monkey API')

        sql_values = apicalls.get_survey_responses(survey_id, question_id, all_response_ids, api_key)
        if sql_values:
            cur.executemany(insert_sql, sql_values)
        conn.commit()

        print(datetime.now(), 'Young Voices database updated')

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(datetime.now(), error)
    finally:
        if conn is not None:
            conn.close()
            print(datetime.now(), 'Database connection closed.')


def select_surveyresponses():
    sent_phonenumbers = 0
    new_entries = []
    unique_phone_numbers = []
    select_sql_sent = "SELECT phonenumber FROM survey_monkey.yv_vitol_char_surveyresponses WHERE senttoapi IS TRUE"
    select_sql = "SELECT id, responseid, phonenumber, datesurveycompleted FROM survey_monkey.yv_vitol_char_surveyresponses WHERE senttoapi IS NULL"
    update_sql = "UPDATE survey_monkey.yv_vitol_char_surveyresponses SET senttoapi = %s WHERE phonenumber = %s"
    conn = None
    try:
        params = database_config()

        print(datetime.now(), 'Connecting to the PostgreSQL database...')

        conn = psycopg2.connect(**params)
        cur = conn.cursor()

        print(datetime.now(), 'Retrieving phone numbers that have not been sent for Airtime Reimbursement')

        cur.execute(select_sql_sent)
        sent_phone_numbers = cur.fetchall()

        for sent_phone_number in sent_phone_numbers:
            cur.execute(update_sql, (True, sent_phone_number))
            conn.commit()

        cur.execute(select_sql)
        rows = cur.fetchall()

        for row in rows:
            if row[2] not in unique_phone_numbers:
                new_entries.append(row)
                unique_phone_numbers.append(row[2])

        print(datetime.now(), len(rows), 'phone numbers to be sent for Airtime Reimbursement')

        for new_entry in new_entries:
            apicalls.send_phone_numbers({
                'tel': new_entry[2],
                'amount': 100,
                'project': 'YVoices1',
                'id': 'YV' + str(new_entry[3].strftime("%Y%m%d%H%M%S")) + '-' + str(new_entry[1])
            })
            cur.execute(update_sql, (True, new_entry[2]))
            conn.commit()
            sent_phonenumbers += 1

        print(datetime.now(), len(rows), 'phone numbers have been sent for Airtime Reimbursement')

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

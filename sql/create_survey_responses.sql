DO
$do$
    BEGIN
        IF NOT EXISTS (
                SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
                WHERE  rolname = 'young-voices') THEN

            CREATE ROLE "young-voices" LOGIN PASSWORD 'YV1234'
                    SUPERUSER
                    CREATEDB
                    CREATEROLE
                    REPLICATION;
        END IF;
    END
$do$;

CREATE SCHEMA IF NOT EXISTS survey_monkey AUTHORIZATION "postgres";

DROP TABLE IF EXISTS survey_monkey."yv_vitol_char_surveyresponses";
DROP FUNCTION IF EXISTS update_timestamp;

CREATE OR REPLACE FUNCTION update_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated = CURRENT_TIMESTAMP;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE survey_monkey."yv_vitol_char_surveyresponses"
(
    id serial NOT NULL,
    responseid character varying NOT NULL,
    phonenumber character varying NOT NULL,
    datesurveycompleted timestamp without time zone NOT NULL,
    senttoapi boolean,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "yv_vitol_char_surveyresponses_pkey" PRIMARY KEY (responseid)
)

TABLESPACE pg_default;

ALTER TABLE survey_monkey."yv_vitol_char_surveyresponses"
    OWNER to "young-voices";

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON survey_monkey.yv_vitol_char_surveyresponses
FOR EACH ROW
EXECUTE PROCEDURE update_timestamp();
from dotenv import load_dotenv
import dbcalls
import sys
import logging
import os
from apscheduler.schedulers.blocking import BlockingScheduler

load_dotenv()

vfr_mac = {
    'question_id': os.getenv("VFR_MAC_QUESTION_ID"),
    'survey_id': os.getenv("VFR_MAC_SURVEY_ID"),
    'api_key': os.getenv("VFR_MAC_ACCESS_TOKEN")
}


vfr_fb = {
    'question_id': os.getenv("VFR_FB_QUESTION_ID"),
    'survey_id': os.getenv("VFR_FB_SURVEY_ID"),
    'api_key': os.getenv("VFR_FB_ACCESS_TOKEN")
}


class Tee(object):
    def __init__(self, *files):
        self.files = files

    def write(self, obj):
        for f in self.files:
            f.write(obj)
            f.flush()  # If you want the output to be visible immediately

    def flush(self):
        for f in self.files:
            f.flush()


f = open('app_log.txt', 'a')

sys.stdout = Tee(sys.stdout, f)

logging.basicConfig(filename='debug_info.log', level=logging.DEBUG, format='%(asctime)s %(message)s')


def survey_monkey_c_grate_sync():
    surveys = [vfr_mac, vfr_fb]
    for survey in surveys:
        dbcalls.insert_into_surveyresponses(survey['survey_id'], survey['question_id'], survey['api_key'])
        dbcalls.select_surveyresponses()


if __name__ == '__main__':
    survey_monkey_c_grate_sync()

    scheduler = BlockingScheduler()
    scheduler.add_job(survey_monkey_c_grate_sync, 'interval', minutes=3)
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass